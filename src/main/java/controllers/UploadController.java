package controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Controller
public class UploadController {

    @PostMapping("/kontingent/")
    @ResponseBody
    public String handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException, InvalidFormatException {

        try (InputStream inputStream = file.getInputStream(); Workbook book = WorkbookFactory.create(inputStream)) {

            Sheet sheet = book.getSheetAt(0);
            Row row = sheet.getRow(0);

            if(row.getCell(0).getCellType() == HSSFCell.CELL_TYPE_STRING){
                String name = row.getCell(0).getStringCellValue();
                System.out.println("name : " + name);
            }

            if(row.getCell(1).getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
                double number = row.getCell(1).getNumericCellValue();
                System.out.println("number :" + number);
            }

            book.close();
        }

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        node.put("status", "ok");
        return node.toString();
    }
}
